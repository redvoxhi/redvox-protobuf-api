#!/usr/bin/env bash

set -o xtrace

echo "Starting build..."

# Check that all dependencies are met
hash protoc 2>/dev/null || { echo >&2 "protoc not found on path. Obtain latest version from https://github.com/google/protobuf/releases"; exit 1; }
#hash protoc-gen-javalite  2>/dev/null || { echo >&2 "protoc-gen-javalite not found on path. Obtain latest version from https://repo1.maven.org/maven2/com/google/protobuf/protoc-gen-javalite/"; exit 1; }
hash pbjs 2>/dev/null || { echo >&2 "pbjs not found on path. Obtain latest version by installing package globally via npm/node. More at https://www.npmjs.com/package/pbjs"; exit 1; }

# requires protobufjs@6.7.0!!!!!! Someone, please update me.

# Define constants
SRC=redvox.proto

GEN=dist
GEN_CPP=${GEN}/cpp
GEN_CSHARP=${GEN}/csharp
GEN_GO=${GEN}/go
GEN_JAVA=${GEN}/java
GEN_JAVASCRIPT=${GEN}/javascript
GEN_OBJC=${GEN}/obj-c
GEN_PYTHON=${GEN}/python
GEN_RUBY=${GEN}/ruby

JS_OUT=redvox.pb.js

# Remove old generated code
rm -rf ${GEN}

# Create directories for newly generated code
mkdir -p ${GEN_CPP}
mkdir -p ${GEN_CSHARP}
mkdir -p ${GEN_JAVA}
mkdir -p ${GEN_JAVASCRIPT}
mkdir -p ${GEN_OBJC}
mkdir -p ${GEN_PYTHON}
mkdir -p ${GEN_RUBY}

echo "Compiling ${SRC} with protoc"
protoc --cpp_out=./${GEN_CPP} \
       --csharp_out=./${GEN_CSHARP} \
       --java_out=./${GEN_JAVA} \
       --objc_out=./${GEN_OBJC} \
       --python_out=./${GEN_PYTHON} \
       --ruby_out=./${GEN_RUBY} \
       ${SRC}

# Generate javascript seperately using 3rd party pbjs
# Requires node, npm, protobufjs to be installed globally
echo "Compiling ${SRC} with pbjs"
pbjs --target static-module --wrap ./jswrapper.js --root redvox --out ./${GEN_JAVASCRIPT}/${JS_OUT} ${SRC}

echo "Done."

set +o xtrace
