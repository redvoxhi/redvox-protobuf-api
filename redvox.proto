syntax = "proto3";

option java_package = "io.redvox.apis.protobuf";
package io.redvox.apis.protobuf;

message RedvoxRequest {
    oneof request {
        // Web requests
        MetadataApi800Request metadataApi800Request = 1;
        DeviceActivityRequest deviceActivityRequest = 2;
        MetadataApi900Request metadataApi900Request = 3;
        DetailsApi900Request detailsApi900Request = 4;
        ActiveDevicesRequest activeDevicesRequest = 5;
        HistoricalDevicesRequest historicalDevicesRequest = 6;
        RdvxzS3Request rdvxzS3Request = 7;

        // Analysis requests
        WebBasedReportRequest webBasedReportRequest = 8;
        RealTimePlotRequest realTimePlotRequest = 9;
        DataRequest dataRequest = 10;
        RealTimePlotRequests realTimePlotRequests = 11;
        RealTimePlotRequestApi900 realTimePlotRequestApi900 = 12;
        RealTimePlotRequestsApi900 realTimePlotRequestsApi900 = 13;
        FcmRequest fcmRequest = 14;
    }

    // meta
    uint64 uuid = 30;
    uint64 timestamp = 31;
    uint64 timeoutSeconds = 32;
    string authenticatedEmail = 33;
    string jsonWebToken = 34;
}

message RedvoxResponse {
    oneof response {
        // Web responses
        MetadataApi800Response metadataApi800Response = 1;
        DeviceActivityResponse deviceActivityResponse = 2;
        MetadataApi900Response metadataApi900Response = 3;
        DetailsApi900Response detailsApi900Response = 4;
        ActiveDevicesResponse activeDevicesResponse = 5;
        HistoricalDevicesResponse historicalDevicesResponse = 6;
        RdvxzS3Response rdvxzS3Response = 7;

        // Analysis responses
        WebBasedReportResponse webBasedReportResponse = 8;
        RealTimePlotResponse realTimePlotResponse = 9;
        ProgressUpdate progressUpdate = 10;
        DataResponse dataResponse = 11;
        RealTimePlotResponseApi900 realTimePlotResponseApi900 = 12;
        FcmResponse fcmResponse = 13;
    }

    // meta
    uint64 uuid = 30;
    uint64 timestamp = 31;
    uint64 timeoutSeconds = 32;
    string authenticatedEmail = 33;
    string jsonWebToken = 34;
}

message RedvoxPacket {
    // Identity information
    uint32 api = 1;                   // The API version of this protocol
    string uuid = 2;                  // A unique identifier assigned by the client and not user configurable
    string redvox_id = 3;             // Device id of the client, user configurable. Alpha-numeric + underscores "_" only.
    string authenticated_email = 4;   // If the client has authenticated, store authenticated email
    string authentication_token = 5;  // JWT obtained from authentication
    string firebase_token = 23;       // Token obtained from Google's Firebase

    // Packet information
    bool is_backfilled = 6; // Is this live data or backfilled (filled in by the server)
    bool is_private = 7;    // Is this data private or public?
    bool is_scrambled = 8;  // Is the audio channel scrambled?

    // Device information
    string device_make = 9;           // e.g. HTC, iPhone, Samsung, etc
    string device_model = 10;         // e.g. PixelXL, 6s, etc
    string device_os = 11;            // e.g. iOS, Android
    string device_os_version = 12;    // Operating system version
    string app_version = 13;          // App version
    float battery_level_percent = 24; // Battery level of device (0.0%-100.0%)
    float device_temperature_c = 25;  // Temperature of device in Celsius

    // Server information
    string acquisition_server = 14;           // Full protocol, url, port, and endpoint. e.g. wss://redvox.io:9000/api/900
    string time_synchronization_server = 15;  // Full protocol, url, port, and endpoint.
    string authentication_server = 16;        // Full protocol, url, port, and endpoint.

    // Timestamps
    int64 app_file_start_timestamp_epoch_microseconds_utc = 17; // Timestamp of packet creation
    int64 app_file_start_timestamp_machine = 18;                // Internal machine time of packet creation
    int64 server_timestamp_epoch_microseconds_utc = 19;         // Time at which this packet arrives at the server (filled in by the server)

    // Data payloads
    repeated EvenlySampledChannel evenly_sampled_channels = 20;      // List of evenly sampled channels. i.e. channels with a stable sample rate such as microphone data
    repeated UnevenlySampledChannel unevenly_sampled_channels = 21;  // List of unevenly sampled channels. i.e. those without a stable sample rate such as barometer or GPS
    repeated string metadata = 22;                                   // Any extra misc metadata that needs associated with this packet
}

// An array of int32s
message Int32Payload {
    repeated int32 payload = 1;
}

// An array of uint32s
message UInt32Payload {
    repeated uint32 payload = 1;
}

// An array of int64s
message Int64Payload {
    repeated int64 payload = 1;
}

// An array of uint64s
message UInt64Payload {
    repeated uint64 payload = 1;
}

// An array of float32s
message Float32Payload {
    repeated float payload = 1;
}

// An array of float64s
message Float64Payload {
    repeated double payload = 1;
}

// An array of bytes
message BytePayload {
    enum BytePayloadType {
        BYTES = 0;
        UINT8 = 1;
        UNINT16 = 2;
        UNINT24 = 3;
        UINT32 = 4;
        UINT64 = 5;
        INT8 = 6;
        INT16 = 7;
        INT24 = 8;
        INT32 = 9;
        INT64 = 10;
        FLOAT32 = 11;
        FLOAT64 = 12;
        OTHER = 13;
    }
    BytePayloadType bytePayloadType = 1; // Optionally specify how the bytes are to be decoded
    bytes payload = 2;
}

enum ChannelType {
    MICROPHONE = 0;
    BAROMETER = 1;
    LATITUDE = 2;
    LONGITUDE = 3;
    SPEED = 4;
    ALTITUDE = 5;
    RESERVED_0 = 6;
    RESERVED_1 = 7;
    RESERVED_2 = 8;
    TIME_SYNCHRONIZATION = 9;
    ACCURACY = 10;
    ACCELEROMETER_X = 11;
    ACCELEROMETER_Y = 12;
    ACCELEROMETER_Z = 13;
    MAGNETOMETER_X = 14;
    MAGNETOMETER_Y = 15;
    MAGNETOMETER_Z = 16;
    GYROSCOPE_X = 17;
    GYROSCOPE_Y = 18;
    GYROSCOPE_Z = 19;
    OTHER = 20;
    LIGHT = 21;
    IMAGE = 22;
    INFRARED = 23;
}

// A channel with evenly sampled data. i.e., one with a stable sample rate such as microphone
// Note: Multiple values can be associated with each channel. If you specify more than one channel type, then the payload should have interleaving values.
// See unevenly sampled channels for a better explanation of this.
message EvenlySampledChannel {
    repeated ChannelType channel_types = 1;                   // Channel types locked to one sample rate
    string sensor_name = 2;                                   // Name of sensor
    double sample_rate_hz = 3;                                // Sample rate in Hz
    int64 first_sample_timestamp_epoch_microseconds_utc = 4;  // Timestamp of first sample in channel
    oneof payload {                                           // Channel payload, client picks most appropriate payload type
                                                              BytePayload byte_payload = 5;
                                                              UInt32Payload uint32_payload = 6;
                                                              UInt64Payload uint64_payload = 7;
                                                              Int32Payload int32_payload = 8;
                                                              Int64Payload int64_payload = 9;
                                                              Float32Payload float32_payload = 10;
                                                              Float64Payload float64_payload = 11;
    }
    repeated double value_means = 12;   // Mean values in payload, one mean per channel
    repeated double value_stds = 13;    // Standard deviations in payload, one per channel
    repeated double value_medians = 14; // Median values in payload, one per channel
    repeated string metadata = 15;      // Extra metadata to associate with this channel
}

// A channel without evenly sampled data. i.e., one with a non-stable sample rate such as barometer or GPS
// Note: Multiple values can be associated with each timestamp such as in the case of a GPS returning lat, lng, speed, and altitude at the same time
// For each value, specify a channel type, then in the payload, interleave the values.
// e.g. channel_types = [LATITUDE, LONGITUDE, SPEED, ALTITUDE], then the payload becomes for each timestamp/sample i
//  payload = [latitude[0], longitude[0], speed[0], altitude[0], latitude[1], longitude[1], speed[1], altitude[1], ..., latitude[i], longitude[i], speed[i], altitude[i]]
message UnevenlySampledChannel {
    repeated ChannelType channel_types = 1;         // Channel types associated with provided timestamps
    string sensor_name = 2;                         // Name of sensor
    repeated int64 timestamps_microseconds_utc = 3; // List of timestamps for each sample
    oneof payload {                                 // Channel payload
                                                    BytePayload byte_payload = 4;
                                                    UInt32Payload uint32_payload = 5;
                                                    UInt64Payload uint64_payload = 6;
                                                    Int32Payload int32_payload = 7;
                                                    Int64Payload int64_payload = 8;
                                                    Float32Payload float32_payload = 9;
                                                    Float64Payload float64_payload = 10;
    }
    double sample_interval_mean = 11;               // Mean of sample internval as determined from timestamps
    double sample_interval_std = 12;                // Standard deviation of sample interval from timestamps
    double sample_interval_median = 13;             // Median of sample interval from timestamps
    repeated double value_means = 14;               // Mean values in payload, one mean per channel
    repeated double value_stds = 15;                // Standard deviations in payload, one per channel
    repeated double value_medians = 16;             // Medians in payload, one per channel
    repeated string metadata = 17;                  // Extra metadata to associate with this channel
}

// Returned to client after each packet send
message RedvoxPacketResponse {
    // Response types
    enum Type {
        OK = 0;
        ERROR = 1;
    }
    // Error types
    enum Error {
        NOT_AUTHENTICATED = 0;
        OTHER = 1;
    }
    Type type = 1;
    int64 checksum = 2;           // This is a sum of the serialized RedvoxPacket bytes
    repeated Error errors = 3;    // List of errors
    repeated string metadata = 4; // Extra metadata to associate with this response
}

// ---------------------------------------------------------- RedVox Web
message RedvoxWebRequest {
    uint64 uuid = 1;
    uint32 timeoutSeconds = 2;
    string requestee = 3;
    oneof request {
        MetadataApi800Request metadataApi800Request = 4;
        DeviceActivityRequest deviceActivityRequest = 5;
        MetadataApi900Request metadataApi900Request = 6;
        DetailsApi900Request detailsApi900Request = 7;
        ActiveDevicesRequest activeDevicesRequest = 8;
        HistoricalDevicesRequest historicalDevicesRequest = 9;
        RdvxzS3Request rdvxzS3Request = 12;
        RdvxJsonRequest rdvxJsonRequest = 13;
    }
    string jsonWebToken = 10;
    string authenticatedEmail = 11;
}

message RedvoxWebResponse {
    uint64 uuid = 1;
    oneof response {
        MetadataApi800Response metadataApi800Response = 2;
        DeviceActivityResponse deviceActivityResponse = 3;
        MetadataApi900Response metadataApi900Response = 4;
        DetailsApi900Response detailsApi900Response = 5;
        ActiveDevicesResponse activeDevicesResponse = 6;
        HistoricalDevicesResponse historicalDevicesResponse = 7;
        RdvxzS3Response rdvxzS3Response = 8;
        StringResponse stringResponse = 9;
    }
    ResponseType responseType = 20;
}

enum ResponseType {
    OK = 0;
    ERROR_NOT_AUTHENTICATED = 1;
    ERROR_TIMEOUT = 2;
    ERROR_OTHER = 30;
}

message MetadataApi800Request {
    uint64 startTimestampSecondsUtc = 1;
    uint64 endTimestampSecondsUtc = 2;
    repeated uint64 deviceIds = 3;

    bool objectId = 4;
    bool deviceId = 5;
    bool deviceUuid = 6;
    bool api = 7;
    bool sizeInBytes = 8;
    bool latitude = 9;
    bool longitude = 10;
    bool altitude = 11;
    bool speed = 12;
    bool calibrationTrim = 13;
    bool deviceEpochUs = 14;
    bool deviceMach = 15;
    bool serverEpochMs = 16;
    bool deviceName = 17;
    bool sensorType = 18;
    bool sensorName = 19;
    bool messageExchanges = 20;
    bool averageRoundTripLatency = 21;
    bool syncServer = 22;
    bool dataServer = 23;
    bool sampleRate = 24;
    bool bitsPerSample = 25;
    bool dataLink = 26;
    bool valid = 27;
}


message MetadataApi900Request {
    string jwtToken = 1;
    string authenticatedUser = 2;
    uint64 startTimestampMsEpochUtc = 3;
    uint64 endTimestampMsEpochUtc = 4;

    repeated string ids = 5;

    bool isOwnedPublic = 6;
    bool isOwnedPrivate = 7;
    bool isOtherPublic = 8;
    Backfilling backfilling = 9;
    Scrambling scrambling = 10;
    repeated string makes = 11;
    repeated string models = 12;
    Oses oses = 13;
    repeated string osVersions = 14;
    repeated string appVersions = 15;
    repeated string dataUrls = 16;
    repeated string synchUrls = 17;
    repeated string authUrls = 18;
    repeated string channels = 19;
    repeated float micSampleRates = 20;

    enum Backfilling {
        NO_BACKFILLING_PREFERENCE = 0;
        BACKFILLED = 1;
        NOT_BACKFILLED = 2;
    }

    enum Scrambling {
        NO_SCRAMBLING_PREFERENCE = 0;
        SCRAMBLED = 1;
        NOT_SCRAMBLED = 2;
    }

    enum Oses {
        NO_OS_PREFERENCE = 0;
        ANDROID = 1;
        IOS = 2;
    }
}

message DetailsApi900Request {
    string jwtToken = 1;
    string authenticatedUser = 2;
    string dataKey = 3;
}

message RdvxzS3Request {
    string dataKey = 1;
}

message RdvxJsonRequest {
    string dataKey = 1;
}

message StringResponse {
    string payload = 1;
}

message DeviceActivityRequest {
    uint64 startTimestampSecondsUtc = 1;
    uint64 endTimestampSecondsUtc = 2;
    uint64 stepSizeSeconds = 3;
    repeated uint64 deviceIds = 4;
    repeated string deviceIdsApi900 = 5;
}

message ActiveDevicesRequest {
    uint64 startTimestampMsEpochUtc = 1;
    uint64 endTimestampMsEpochUtc = 2;
    repeated string deviceIds = 3;
    string authenticatedEmail = 4;
}

message ActiveDevicesResponse {
    repeated RedvoxDevice redvoxDevices = 1;
}

message HistoricalDevicesRequest {
    bool includeOwned = 1;
}

message HistoricalDevicesResponse {
    repeated RedvoxDevice redvoxDevices = 1;
}

message RedvoxDevice {
    string redvoxId = 1;
    string uuid = 2;
    uint64 lastActiveTimestampMsEpochUtc = 3;
    double lastLatitude = 4;
    double lastLongitude = 5;
    string os = 6;
    string authenticatedEmail = 7;
    PrivacyPolicy privacyPolicy = 8;
    string firebaseToken = 9;
}

enum PrivacyPolicy {
    PUBLIC = 0;
    PRIVATE = 1;
}

message DeviceActivityResponse {
    string payload = 1;
}

message MetadataApi800Response {
    string url = 2;
}

message MetadataApi900Response {
    repeated RedvoxPacket redvoxPackets = 1;
}

message DetailsApi900Response {
    RedvoxPacket redvoxPacket = 1;
}

message RdvxzS3Response {
    bytes rdvxz = 1;
}

// ---------------------------------------------------------- RedVox Analysis
message Response {
    enum ResponseType {
        SUCCESS = 0;
        ERROR = 1;
        DONE = 2;
    }

    ResponseType responseType = 1;
    string message = 2;
    string error = 3;
}

enum PlotType {
    MICROPHONE_FFT = 0;
    MICROPHONE_MULTIRES = 1;
    BAROMETER_FFT = 2;
    BAROMETER_MULTIRES = 3;
    LATENCY = 4;
    LATITUDE_PLOT = 5;
    LONGITUDE_PLOT = 6;
    ALTITUDE_PLOT = 7;
    ACCURACY_PLOT = 8;
    MAGNETOMETER_PLOT = 9;
    GYROSCOPE_PLOT = 10;
    LIGHT_PLOT = 11;
    ACCELEROMETER_PLOT = 12;
    MICROPHONE_FFT_LINEAR = 13;
    MICROPHONE_FFT_LOG = 14;
    AUDIO_PICKER = 15;
}

enum DataType {
    WAVE_MIC = 0;
    WAVE_BAR = 1;
}

message WebBasedReportRequest {
    uint64 uuid = 1;
    uint64 startTimestampSecondsUtc = 2;
    uint64 endTimestampSecondsUtc = 3;
    repeated uint64 deviceIds = 4;
    repeated PlotType plotTypes = 5;
    double originLatitudeDecimalDegrees = 6;
    double originLongitudeDecimcalDegrees = 7;
    double originHeightMeters = 8;
    uint64 originTimestampSecondsUtc = 9;
    string requestee = 10;
    string reportId = 11;
    repeated string deviceIdsApi900 = 12; // redvoxId:redvoxUuid
    bool performTimeSynch = 13;
    string params = 14;
}

message WebBasedReportResponse {
    uint64 uuid = 1;
    string reportId = 2;
    Response response = 3;
}

message ProgressUpdate {
    uint64 uuid = 1;
    uint32 currentStage = 2;
    uint32 totalStages = 3;
    string messageStage = 4;
    uint32 currentItem = 5;
    uint32 totalItems = 6;
    string messageItem = 7;
    string meta = 8;
}

message DataRequest {
    uint64 uuid = 1;
    uint64 startTimestampSecondsUtc = 2;
    uint64 endTimestampSecondsUtc = 3;
    repeated uint64 deviceIds = 4;
    repeated DataType dataTypes = 5;
}

message DataResponse {
    uint64 uuid = 1;
    string dataKey = 2;
    Response response = 3;
}

message RealTimePlotRequest {
    uint64 uuid = 1;
    uint64 deviceId = 2;
    PlotType plotType = 3;
    uint64 secondsBeforeNow = 4;
}

message RealTimePlotResponse {
    uint64 uuid = 1;
    uint64 deviceId = 2;
    PlotType plotType = 3;
    Response response = 4;
}

message RealTimePlotRequests {
    uint64 uuid = 1;
    repeated RealTimePlotRequest realTimePlotRequests = 2;
}

message RealTimePlotRequestApi900 {
    string redvoxId = 1;
    string redvoxUuid = 2;
    PlotType plotType = 3;
    uint64 secondsBeforeNow = 4;
}

message RealTimePlotResponseApi900 {
    string redvoxId = 1;
    string redvoxUuid = 2;
    PlotType plotType = 3;
    Response response = 4;
    string dataKey = 5;
}

message RealTimePlotRequestsApi900 {
    repeated RealTimePlotRequestApi900 realTimePlotRequestsApi900 = 1;
}

message FcmRequest {
    uint64 uuid = 1;
    string title = 2;
    string body = 3;
    repeated string fcmTokens = 4;
}

message FcmResponse {
    uint64 uuid = 1;
    repeated string responses = 2;
}


message AnalysisRequest {
    uint64 uuid = 1;
    uint32 timeoutSeconds = 2;
    oneof request {
        WebBasedReportRequest webBasedReportRequest = 3;
        RealTimePlotRequest realTimePlotRequest = 4;
        DataRequest dataRequest = 5;
        RealTimePlotRequests realTimePlotRequests = 6;
        RealTimePlotRequestApi900 realTimePlotRequestApi900 = 7;
        RealTimePlotRequestsApi900 realTimePlotRequestsApi900 = 8;
        FcmRequest fcmRequest = 9;
    }
    string authenticatedEmail = 20;
    string authenticationToken = 21;
    uint64 timestampEpochSeconds = 22;
}

message AnalysisResponse {
    uint64 uuid = 1;
    oneof response {
        WebBasedReportResponse webBasedReportResponse = 2;
        RealTimePlotResponse realTimePlotResponse = 3;
        ProgressUpdate progressUpdate = 4;
        DataResponse dataResponse = 5;
        RealTimePlotResponseApi900 realTimePlotResponseApi900 = 6;
        FcmResponse fcmResponse = 7;
    }
    AnalysisRequest originalRequest = 20;
    uint64 timestampEpochSeconds = 21;
}
